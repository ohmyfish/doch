Typed a command but forgot to use `sudo`? Then `doch` will be useful to you.

# <img src="https://cdn.rawgit.com/oh-my-fish/oh-my-fish/e4f1c2e0219a17e2c748b824004c8d0b38055c16/docs/logo.svg" width="28px" height="28px"/> doch

A plugin for [Oh My Fish][oh-my-fish]. 

[![MIT License](https://img.shields.io/badge/License-MIT-blue?style=for-the-badge)](LICENSE.md)
[![Fish Shell](https://img.shields.io/badge/fish-3.1.0-blue?style=for-the-badge)](https://fishshell.com)
[![Oh My Fish](https://img.shields.io/badge/Oh%20My%20Fish-Fishshell--Framework-blue?style=for-the-badge)](https://github.com/oh-my-fish/oh-my-fish)

**doch** is a german word to respond **yes I can** after someone says no.

## Install

```shell script
$ omf install doch
```

## Usage

Repeat the last command as sudo

```shell script
$ touch /etc/test
touch: Impossible to touch '/etc/test': Permission denied
$ doch
$ ls /etc/test
/etc/test
```

## Notes

This plugin is a clone of another one created by [Ruben van Vreeland][original], but **SFW**.

# License

[MIT][mit] © [Marc-André Appel][author]

[oh-my-fish]: https://www.github.com/oh-my-fish/oh-my-fish
[original]: https://github.com/rubiev
[author]: https://gitlab.com/marc-andre
[mit]: /LICENSE.md
